package dali.hmida.mindorkshilttuto.data.repository

import dali.hmida.mindorkshilttuto.data.api.ApiHelper
import javax.inject.Inject


class MainRepository @Inject constructor(private val apiHelper: ApiHelper) {
    suspend fun getUsers() =  apiHelper.getUsers()
}