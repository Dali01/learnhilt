package dali.hmida.mindorkshilttuto.data.api

import dali.hmida.mindorkshilttuto.data.model.User
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET("users")
    suspend fun getUsers(): Response<List<User>>
}