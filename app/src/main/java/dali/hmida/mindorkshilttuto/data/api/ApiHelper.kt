package dali.hmida.mindorkshilttuto.data.api

import dali.hmida.mindorkshilttuto.data.model.User
import retrofit2.Response

interface ApiHelper {

    suspend fun getUsers(): Response<List<User>>
}