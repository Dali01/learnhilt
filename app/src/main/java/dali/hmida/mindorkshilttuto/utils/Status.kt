package dali.hmida.mindorkshilttuto.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}